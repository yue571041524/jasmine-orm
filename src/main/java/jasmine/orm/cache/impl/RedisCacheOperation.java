package jasmine.orm.cache.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.springframework.data.redis.core.RedisTemplate;


import jasmine.orm.cache.CacheOperation;
import jasmine.orm.util.StrUtils;

/**
 *  redis 缓存操作
 * @author hanjiang.Yue
 *
 */
@SuppressWarnings("unchecked")
public class RedisCacheOperation implements CacheOperation{

	private RedisTemplate<String, Object> redisTemplate;
	
	@Override
	public <T> T get(Class<T> typeClass, String cacheKey) {
		
		return (T)redisTemplate.opsForValue().get(cacheKey);
	}

	@Override
	public void put(String cacheKey, Object value) {
		redisTemplate.opsForValue().set(cacheKey,value);
	}

	@Override
	public void put(String cacheKey, long expiryTime, Object value) {
		if(expiryTime > 0) {
			redisTemplate.opsForValue().set(cacheKey, value, expiryTime,TimeUnit.SECONDS);
			return;
		}
		this.put(cacheKey, value);
		
	}

	@Override
	public void delete(String cacheKey) {
//		System.out.println(redisTemplate.getClass());
		redisTemplate.delete(cacheKey);
	}

	@Override
	public void delete(Collection<String> cacheKeys) {
		redisTemplate.delete(cacheKeys);
	}


	@Override
	public <T> List<T> list(Class<T> type, Collection<String> keys) {
		return (List<T>)redisTemplate.opsForValue().
				multiGet(keys).
				stream().
				filter(data->!StrUtils.isEmpty(data)).
				collect(Collectors.toList());
	}

	@Override
	public <T> List<T> list(Class<T> type, String cacheKey) {
		return (List<T>)redisTemplate.opsForValue().get(cacheKey);
	}


	@Override
	public void put(Map<String, Object> elements, long expiryTime) {
		
		redisTemplate.opsForValue().multiSet(elements);
		
//		RedisSerializer<String> keySerializer = (RedisSerializer<String>) redisTemplate.getKeySerializer();
//		RedisSerializer<Object> valueSerializer = (RedisSerializer<Object>) redisTemplate.getValueSerializer();
//		redisTemplate.executePipelined(new RedisCallback<Object>() {
//			@Override
//			public Object doInRedis(RedisConnection connection) throws DataAccessException {
//				elements.forEach((key,value)->{
//					connection.setEx(keySerializer.serialize(key), expiryTime,valueSerializer.serialize(value));
//				});
//				return null;
//			}
//		});
		
	}


	public RedisCacheOperation(RedisTemplate<String, Object> redisTemplate) {
		this.redisTemplate = redisTemplate;
		
	}

	
	
}
