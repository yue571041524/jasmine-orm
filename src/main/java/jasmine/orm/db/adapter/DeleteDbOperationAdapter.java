package jasmine.orm.db.adapter;


import jasmine.orm.code.DbContext;
import jasmine.orm.query.Query;

public class DeleteDbOperationAdapter<T> extends AbstractDbOperationAdapter<T>{

	public DeleteDbOperationAdapter(DbContext dbContext, Query<T> query) {
		super(dbContext, query);
	}

	@Override
	public Object cacheAdapter() {
		String cacheKey = query.getCacheKey();
		synchronized (cacheKey) {
			cacheOperation.delete(cacheKey);
			return dbAdapter();
		}
		
	}

	@Override
	public Object dbAdapter() {
		String sql = query.getQueryBuilder().buildDeleteSQL();
		if(log.isInfoEnabled() && context.isShowSql()) {
			log.info("==> execute DELETE [sql={},params={}]",sql,query.getParams());
		}
		return  dbOperation.update(sql, query.getParams().toArray());
	}









	

}
