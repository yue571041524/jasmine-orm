package jasmine.orm.db.adapter;

import java.util.Map;

import jasmine.orm.code.DbContext;
import jasmine.orm.query.Query;

/**
 * 软删除
 * @author yue
 *
 * @param <T>
 */
public class SoftDeleteByIdDbOperationAdapter<T> extends UpdateEntityMapDbOperationAdapter<T>{

	public SoftDeleteByIdDbOperationAdapter(DbContext dbContext, Query<T> query, Map<String, Object> entityMap) {
		super(dbContext, query, entityMap);
		
		
	}

	
	

}
