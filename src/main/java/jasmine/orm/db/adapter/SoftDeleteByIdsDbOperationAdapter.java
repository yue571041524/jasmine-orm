package jasmine.orm.db.adapter;

import java.util.List;
import java.util.stream.Collectors;

import jasmine.orm.code.DbContext;
import jasmine.orm.query.Query;

public class SoftDeleteByIdsDbOperationAdapter<T,Id> extends UpdateDbOperationAdapter<T>{

	private List<Id> ids;
	
	public SoftDeleteByIdsDbOperationAdapter(DbContext context, Query<T> query,List<Id> ids) {
		
		super(context, query);
		this.ids = ids;
		query.set(tableMapping.getSoftDeleteName(), 1).where().idIn(ids.toArray());
		if(tableMapping.isCache()) {
			query.cache("",tableMapping.getCacheTime());
		}
		
	}

	
	@Override
	public Object cacheAdapter() {
		
		
		List<String> cacheIds = ids.stream().map(x->query.getCacheKey(x.toString())).distinct().collect(Collectors.toList());
		
		cacheOperation.delete(cacheIds);
		
		return dbAdapter();
		
	}

}
