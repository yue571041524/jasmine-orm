package jasmine.orm.query;

@FunctionalInterface
public interface QueryAndThenFunc<Q,V> {

	 void accept(Q q,V v);
}
